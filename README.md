# README #

If you want this application to be able to start on multiple machines on the same network, you have to  add your ip address to /server/connection.js and /public/app.js. 

1.  /server/connection.js

    const host = process.env.ES_HOST || 'localhost'  || http://192.168.0.105

2. /public/app.js

    baseUrl: 'http://192.168.0.105:3000', // API url

From the root folder, start cmd and run docker command: **docker-compose build**

From the root folder, start cmd and run docker command: **docker-compose up**

While the app and Elasticsearch instances are started, open another cmd from /server and run command for bulking data into Elasticsearch.

**curl -XPOST -H "Content-Type: application/json" "localhost:9202/library/_bulk" --data-binary @books1.json**

**curl -XPOST -H "Content-Type: application/json" "localhost:9201/library/_bulk" --data-binary @books1.json**

**curl -XPOST -H "Content-Type: application/json" "localhost:9200/library/_bulk" --data-binary @books1.json**

Finally, run frontend application at  http://192.168.0.105:8080



