const fs = require('fs')
const path = require('path')
const esConnection = require('./connection')

/** Read an individual book txt file, and extract the title, author, and paragraphs */
function parseBookFile (filePath) {
  // Read text file
  const book = fs.readFileSync(filePath, 'utf8')

  // Find book title and author
  const title = book.match(/^Title:\s(.+)$/m)[1]
  const authorMatch = book.match(/^Author:\s(.+)$/m)
  const author = (!authorMatch || authorMatch[1].trim() === '') ? 'Unknown Author' : authorMatch[1]

  console.log(`Reading Book - ${title} By ${author}`)

  // Find Guttenberg metadata header and footer
  const startOfBookMatch = book.match(/^\*{3}\s*START OF (THIS|THE) PROJECT GUTENBERG EBOOK.+\*{3}$/m)
  const startOfBookIndex = startOfBookMatch.index + startOfBookMatch[0].length
  const endOfBookIndex = book.match(/^\*{3}\s*END OF (THIS|THE) PROJECT GUTENBERG EBOOK.+\*{3}$/m).index

  // Clean book text and split into array of paragraphs
  const paragraphs = book
    .slice(startOfBookIndex, endOfBookIndex) // Remove Guttenberg header and footer
    .split(/\n\s+\n/g) // Split each paragraph into it's own array entry
    .map(line => line.replace(/\r\n/g, ' ').trim()) // Remove paragraph line breaks and whitespace
    .map(line => line.replace(/_/g, '')) // Guttenberg uses "_" to signify italics.  We'll remove it, since it makes the raw text look messy.
    .filter((line) => (line && line !== '')) // Remove empty lines

  console.log(`Parsed ${paragraphs.length} Paragraphs\n`)
  return { title, author, paragraphs }
}

async function insertBookData (title, author, paragraphs) {
   let bulkOps = [] // Array to store bulk operations

  // Add an index operation for each section in the book
  for (let i = 0; i < paragraphs.length; i++) {
    // Add index
    bulkOps.push({ index: { _index: esConnection.index, _type: esConnection.type } })
	
    // Add document
    bulkOps.push({
      author : author,
      title : title,
      location: i,
      text: paragraphs[i]
    })

    if (i > 0 && i % 500 === 0) {
	var path = require('path');
	var root = path.dirname(require.main.filename);
	fs.writeFile('./server/books3.json', JSON.stringify(bulkOps), { flag: 'w' }, function(err) {
    if (err) 
        return console.error(err); 
    fs.readFile('./server/books3.json', 'utf-8', function (err, data) {
        if (err)
            return console.error(err);
        console.log(data);
    });
	});
      bulkOps = []
    }
  }
}
async function readAndCreateJson () {

  try {
    let files1 = fs.readdirSync('./books1').filter(file => file.slice(-4) === '.txt')
    console.log(`Found ${files1.length} Files`)

    for (let file of files1) {
      console.log(`Reading File - ${file}`)
      const filePath = path.join('./books1', file)
	  
      const { title, author, paragraphs } = parseBookFile(filePath)
	  
	  insertBookData(title, author, paragraphs)
    }
  } catch (err) {
    console.error(err)
  } 
}

readAndCreateJson()
