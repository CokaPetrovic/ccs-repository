const { client, index, type } = require('./connection')
module.exports = {	
count () {
  const result = client.count({ index, type })
  console.log('count', result)
},
testSearch () {
var i
var start = new Date().getTime();

for (i = 0;i<100;i++){
  const { body } = client.msearch({
    body: [
      { index: 'library' },
      { query: { bool: {
			must:[
			{
			terms:{
				'Text':[
				"they", "jack"]
				
			}}
			],
			should:[
			{
				range:{
					'location':{
						"gte":10,
						"lte":40
					}
				}
			}]
			} } },

      { index: 'library' },
      { query: { bool: {
			must:[
			{
			terms:{
				'Text':
				[
				"Tell", "board"]
			}}
			],
			should:[
			{
				range:{
					'location':{
						"gte":10,
						"lte":40
					}
				}
			}]
			} } },
		      { index: 'library' },
      { query: { bool: {
			must:[
			{
			terms:{
				'Text':
				[
				"they", "how"]
			}}
			],
			should:[
			{
				range:{
					'location':{
						"gte":10,
						"lte":40
					}
				}
			}]
			} } },
			      { index: 'library' },
      { query: { bool: {
			must:[
			{
			terms:{
				'Text':
				[
				"they", "jack"]
			}}
			],
			should:[
			{
				range:{
					'location':{
						"gte":10,
						"lte":40
					}
				}
			}]
			} } },
			{ index: 'library' },
      { query: { bool: {
			must:[
			{
			terms:{
				'Text':
				[
				"they", "how"]
			}}
			],
			should:[
			{
				range:{
					'location':{
						"gte":10,
						"lte":40
					}
				}
			}]
			} } },
	  { index: 'library' },
      { query: { bool: {
			must:[
			{
			terms:{
				'Text':
				[
				"bell"]
			}}
			]
			} } },
	  { index: 'library' },
      { query: { bool: {
			must:[
			{
			terms:{
				'Text':
				[
				"how"]
			}}
			]
			} } }
    ]
  })
}
var end = new Date().getTime();
return (end-start)
// console.log(body.responses)
}

,
testSearch2 () {
   var i
var start = new Date().getTime();

for (i = 0;i<100;i++){
  const { body } = client.msearch({
    body: [
      { index: 'library,cluster_one:library,cluster_two:library' },
      { query: { bool: {
			must:[
			{
			terms:{
				'Text':[
				"they", "jack"]
				
			}}
			],
			should:[
			{
				range:{
					'location':{
						"gte":10,
						"lte":40
					}
				}
			}]
			} } },

      { index: 'library,cluster_one:library,cluster_two:library' },
      { query: { bool: {
			must:[
			{
			terms:{
				'Text':
				[
				"they", "jack"]
			}}
			],
			should:[
			{
				range:{
					'location':{
						"gte":10,
						"lte":40
					}
				}
			}]
			} } },
			      { index: 'library,cluster_one:library,cluster_two:library' },
      { query: { bool: {
			must:[
			{
			terms:{
				'Text':
				[
				"they", "jack"]
			}}
			],
			should:[
			{
				range:{
					'location':{
						"gte":10,
						"lte":40
					}
				}
			}]
			} } },
			      { index: 'library,cluster_one:library,cluster_two:library' },
      { query: { bool: {
			must:[
			{
			terms:{
				'Text':
				[
				"tell", "board"]
			}}
			],
			should:[
			{
				range:{
					'location':{
						"gte":10,
						"lte":40
					}
				}
			}]
			} } },
			{ index: 'library,cluster_one:library,cluster_two:library' },
      { query: { bool: {
			must:[
			{
			terms:{
				'Text':
				[
				"they", "how"]
			}}
			],
			should:[
			{
				range:{
					'location':{
						"gte":10,
						"lte":40
					}
				}
			}]
			} } },
	{ index: 'library,cluster_one:library,cluster_two:library' },
      { query: { bool: {
			must:[
			{
			terms:{
				'Text':
				[
				"bell"]
			}}
			]
			} } },
	  { index: 'library,cluster_one:library,cluster_two:library' },
      { query: { bool: {
			must:[
			{
			terms:{
				'Text':
				[
				"how"]
			}}
			]
			} } }
    ]
  })
}
  var end = new Date().getTime();
return (end-start)
}}

async function main () {
  await count()
  var i;
  var start = new Date().getTime();
  for (i = 0;i<100;i++){
  await testSearch()
  }
  var end = new Date().getTime();
  
  
  var start2 = new Date().getTime();
  for (i = 0;i<100;i++){
  await testSearch2()
  }
  var end2 = new Date().getTime();
  
  
    console.log('Trebalo je za obicno ' + (end-start) +'ms')

  console.log('Trebalo je za multicluster ' + (end2-start2) +'ms')
}

main()
