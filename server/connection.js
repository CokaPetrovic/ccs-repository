const elasticsearch = require('elasticsearch')

// Core ES variables for this project
const index = 'library'
const type = 'book'
const port1 = 9200
//const port2 = 9201
//const port3 = 9202

const host = process.env.ES_HOST || 'localhost'
const client = new elasticsearch.Client({ host: { host, port1 } })
//const client2 = new elasticsearch.Client({ host: {host, port2}})
//const client3 = new elasticsearch.Client({ host: {host, port3}})


/** Check the ES connection status */
async function checkConnection () {
  let isConnected = false
  while (!isConnected) {
    console.log('Connecting to ES')
    try {
      const health = await client.cluster.health({})
      console.log(health)
      isConnected = true
    } catch (err) {
      console.log('Connection Failed, Retrying...', err)
    }
  }  
}

/** Clear the index, recreate it, and add mappings */
async function resetIndex () {
  if (await client.indices.exists({ index })) {
    await client.indices.delete({ index })
  }

  await client.indices.create({ 
  index:index,
	body:{
		settings: {
			index:
			{
				soft_deletes:
				{
					enabled:true
				}
			}
		}
	}})
	
	
	  if (await client.indices.exists({ index })) {
    await client.indices.delete({ index })
  }

  await putBookMapping()
}

/** Add book section schema mapping to ES */
async function putBookMapping () {
  const schema = {
    title: { type: 'keyword' },
    author: { type: 'keyword' },
    location: { type: 'integer' },
    text: { type: 'text' }
  }

  return client.indices.putMapping({ 
  index:index, 
  type:type, 
  body: { properties: schema },
  include_type_name:true
  })
}

module.exports = {
  client, index, type, checkConnection, resetIndex
}

